package com.ajay.filereadtest;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ajay.filereadtest.permissionhandler.OnPermissionResponseCallback;
import com.ajay.filereadtest.permissionhandler.PermissionHandler;
import com.ajay.filereadtest.permissionhandler.PermissionHandlerBuilder;

import java.io.File;

public class MainActivity extends AppCompatActivity implements OnPermissionResponseCallback {

    private static final int CHOOSE_FILE_REQUESTCODE = 123;
    private static final int REQUEST_CODE_STORAGE_PERMISSION = 232;
    private PermissionHandler permissionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        permissionHandler = PermissionHandlerBuilder.newBuilder()
                .with(this)
                .forStoragePermission()
                .noMorePermission()
                .listenWith(this)
                .fromActivity(this)
                .build();


        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionHandler.request(REQUEST_CODE_STORAGE_PERMISSION);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CHOOSE_FILE_REQUESTCODE:
                Uri uri = data.getData();
                File file = new File(FileUtils.getPath(this, uri));
                if (file.exists()) {
                    Toast.makeText(this, "exists", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "doesnt exist", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.handlePermissionResponse(requestCode, grantResults);
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        Intent i = Intent.createChooser(intent, "File");
        startActivityForResult(i, CHOOSE_FILE_REQUESTCODE);
    }

    @Override
    public void onPermissionRejected(int requestCode) {
        Toast.makeText(this, "Grant permissions", Toast.LENGTH_LONG).show();
    }
}
