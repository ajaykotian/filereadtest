package com.ajay.filereadtest.permissionhandler;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;


import java.util.ArrayList;
import java.util.List;

/**
 * Class that contains method to ask for various permission that are not granted from user for the current application.
 */
public class PermissionHandler {

    private Context context;
    private List<String> permissions;
    private OnPermissionResponseCallback onPermissionResponseCallback;
    private Activity activityInstance;
    private Fragment fragmentInstance;
    private int requestCode;

    PermissionHandler(PermissionHandlerBuilder.PermissionSteps permissionSteps) {
        context = permissionSteps.getContext();
        permissions = new ArrayList<>();
        permissions.addAll(permissionSteps.getPermissions());
        onPermissionResponseCallback = permissionSteps.getPermissionHandlerIntf();
        activityInstance = permissionSteps.getActivity();
        fragmentInstance = permissionSteps.getFragment();
    }

    public void request(int requestCode) {
        this.requestCode = requestCode;
        onPermissionRequested(requestCode);
    }


    private void onPermissionRequested(int requestCode) {
        if (isPermissionListGranted(context, permissions)) {
            onPermissionResponseCallback.onPermissionGranted(requestCode);
        } else {
            askForPermission(requestCode);
        }
    }

    private void askForPermission(int requestCode) {
        if (fragmentInstance != null) {
            fragmentInstance.requestPermissions(permissions.toArray(new String[permissions.size()]), requestCode);
        } else if (activityInstance != null) {
            ActivityCompat.requestPermissions(activityInstance, permissions.toArray(new String[permissions.size()]), requestCode);
        }
    }


    private boolean isPermissionListGranted(Context context, List<String> permissionList) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            for (String permission : permissionList) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean handlePermissionResponse(int actualRequestCode, int[] grantResults) {

        boolean isPermissionGranted = true;
        boolean isRequiredCodeSameAsPassedCode = false;

        if (actualRequestCode == requestCode) {

            for (int grantStatus : grantResults) {
                if (grantStatus == PackageManager.PERMISSION_DENIED) {
                    isPermissionGranted = false;
                    break;
                }
            }

            if (isPermissionGranted) {
                onPermissionResponseCallback.onPermissionGranted(requestCode);
            } else {
                onPermissionResponseCallback.onPermissionRejected(requestCode);
            }

            isRequiredCodeSameAsPassedCode = true;

        }

        return isRequiredCodeSameAsPassedCode;
    }
}
