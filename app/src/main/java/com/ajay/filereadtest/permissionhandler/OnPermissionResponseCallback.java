package com.ajay.filereadtest.permissionhandler;

/**
 * Callback that is called when user accepts/rejects the permission asked.
 */
public interface OnPermissionResponseCallback {

    /**
     * Method called when the user grants the permission to the current application.
     */
    void onPermissionGranted(int requestCode);

    /**
     * Method called when the user rejects the permission to the current application.
     */
    void onPermissionRejected(int requestCode);
}
