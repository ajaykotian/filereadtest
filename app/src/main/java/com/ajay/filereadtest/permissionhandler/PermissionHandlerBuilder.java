package com.ajay.filereadtest.permissionhandler;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class to generate {@link PermissionHandler}.
 */
public class PermissionHandlerBuilder {

    public static ContextStep newBuilder() {
        return new PermissionSteps();
    }

    public interface ContextStep {
        PermissionListStep with(Context context);
    }

    public interface PermissionListStep {
        PermissionListStep forPermission(String permission);

        PermissionListStep forCameraPermission();

        PermissionListStep forStoragePermission();

        PermissionListStep forLocationPermission();

        CallBackStep noMorePermission();
    }

    public interface CallBackStep {
        VisibleViewComponentStep listenWith(OnPermissionResponseCallback onPermissionResponseCallback);
    }

    public interface VisibleViewComponentStep {
        BuildStep fromActivity(Activity activity);

        BuildStep fromFragment(Fragment fragment);
    }

    public interface BuildStep {
        PermissionHandler build();
    }

    public static class PermissionSteps implements ContextStep, PermissionListStep, CallBackStep, VisibleViewComponentStep, BuildStep {

        private Context context;
        private List<String> permissions;
        private OnPermissionResponseCallback onPermissionResponseCallback;
        private Activity activity;
        private Fragment fragment;

        private PermissionSteps() {
            this.permissions = new ArrayList<>();
        }

        @Override
        public PermissionListStep with(Context context) {
            this.context = context;
            return this;
        }

        @Override
        public PermissionListStep forPermission(String permission) {
            permissions.add(permission);
            return this;
        }

        @Override
        public PermissionListStep forCameraPermission() {
            permissions.add(Manifest.permission.CAMERA);
            return this;
        }

        @Override
        public PermissionListStep forStoragePermission() {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return this;
        }

        @Override
        public PermissionListStep forLocationPermission() {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            return this;
        }

        @Override
        public CallBackStep noMorePermission() {
            return this;
        }

        @Override
        public VisibleViewComponentStep listenWith(OnPermissionResponseCallback onPermissionResponseCallback) {
            this.onPermissionResponseCallback = onPermissionResponseCallback;
            return this;
        }

        @Override
        public BuildStep fromActivity(Activity activity) {
            this.activity = activity;
            return this;
        }

        @Override
        public BuildStep fromFragment(Fragment fragment) {
            this.fragment = fragment;
            return this;
        }

        @Override
        public PermissionHandler build() {
            return new PermissionHandler(this);
        }

        public Context getContext() {
            return context;
        }

        public List<String> getPermissions() {
            return permissions;
        }

        public OnPermissionResponseCallback getPermissionHandlerIntf() {
            return onPermissionResponseCallback;
        }

        public Activity getActivity() {
            return activity;
        }

        public Fragment getFragment() {
            return fragment;
        }
    }
}
